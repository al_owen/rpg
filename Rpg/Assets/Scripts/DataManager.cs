﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// static class to save some persistent runningtime data
/// </summary>
public static class DataManager
{
    public static float volumen = 0;
    public static int EnemyIndex = 0;
    public static Turn currentTurn;
}
