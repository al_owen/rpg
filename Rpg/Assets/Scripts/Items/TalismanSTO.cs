﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// scriptables objects of talismans
/// </summary>
[CreateAssetMenu (fileName = "talisman", menuName="ItemsSTO/talisman")]
public class TalismanSTO : ItemsSTO
{
    public TalismanEffects talismanEffects;
    public int effect;
}
