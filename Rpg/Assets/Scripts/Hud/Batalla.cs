﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// Script that controlls the battle aspects of the battleground
/// </summary>
public class Batalla : MonoBehaviour
{
    //public variables
    public Text dialog;
    public List<PersonajeSTO> aliados = new List<PersonajeSTO>();
    public List<EnemigoSTO> enemigos = new List<EnemigoSTO>();
    public InventarioSTO inventory;
    public GameEventSTO endTurn;
    public GameObject highlight;
    public Vector3[] enemiesPositions = new Vector3[3];

    //static lists that stores the current allies and enemies
    public static List<PersonajeSTO> allies;
    public static List<EnemigoSTO> enemies;

    //private variables to handle the battle
    private Turn character;
    private bool canAtack;
    private bool endBatle;
    private int enemyIndex = 0;
    private int maxIndex = 2;
    private LevelManager lvlManager;

    void Awake()
    {
        lvlManager = this.gameObject.GetComponent<LevelManager>();
        allies = new List<PersonajeSTO>();
        enemies = new List<EnemigoSTO>();
    }
    private void Start()
    {
        allies.AddRange(aliados);
        enemies.AddRange(enemigos);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            selectTarget("down");
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            selectTarget("up");
        }
    }

    /// <summary>
    /// moves the target selector 
    /// </summary>
    /// <param name="position">index of the position to be moved</param>
    public void selectTarget(string position)
    {
        switch (position)
        {
            case "down":
                if (enemyIndex < maxIndex)
                {
                    enemyIndex++;
                }
                else
                {
                    enemyIndex = 0;
                }
                break;
            case "up":
                if (enemyIndex > 0)
                {
                    enemyIndex--;
                }
                else
                {
                    enemyIndex = maxIndex;
                }
                break;
        }
        highlight.transform.position = enemiesPositions[enemyIndex];
    }

    /// <summary>
    /// selects which ability will the ally use, based on the passed index
    /// </summary>
    /// <param name="habilityIndex">index of the ability that we will use</param>
    public void habilities(int habilityIndex)
    {
        PersonajeSTO ally = getAlly();
        int finalDamage = ally.abilities[habilityIndex].damage;
        int energyUsed = ally.abilities[habilityIndex].costeEnergia;
        if (ally.useEnergy(energyUsed))
        {
            StartCoroutine(attackAction());
            getEnemy().beDamaged(finalDamage);
            endTurn.Raise();
        }
        
    }

    /// <summary>
    /// returns the ally depending on the turn
    /// </summary>
    /// <returns>current ally</returns>
    public PersonajeSTO getAlly()
    {
        switch (LevelManager.CurrentTurn)
        {
            case Turn.Ezio:
                return aliados[0];
            case Turn.Alfar:
                return aliados[1];
            case Turn.Scarlet:
                return aliados[2];
            case Turn.Alexia:
                return aliados[3];
        }
        return null;
    }

    /// <summary>
    /// returns the enemy depending on the turn
    /// </summary>
    /// <returns>current enemy</returns>
    public EnemigoSTO getEnemy()
    {
        switch (enemyIndex)
        {
            case 0:
                return enemigos[0];
            case 1:
                return enemigos[1];
            case 2:
                return enemigos[2];
        }
        return null;
    }

    /// <summary>
    /// uses an item
    /// </summary>
    /// <param name="item">item to be used</param>
    public void useItem(PotionsSTO item)
    {
        if (getAlly().usePotion(item))
        {
            endTurn.Raise();
        }
    }

    /// <summary>
    /// attempts to scape, if successful leaves the area if not, then changes turn
    /// </summary>
    public void run()
    {
        int r = Random.Range(1, 4);
        if (r == 3)
        {
            SceneManager.LoadScene("World");
        }
        else
        {
            dialog.gameObject.SetActive(true);
            dialog.text = "You can't scape us!";
            StartCoroutine(fadeText());
            endTurn.Raise();
        }
    }

    /// <summary>
    /// checks if the battle is over
    /// </summary>
    public void endBattle()
    {
        int faintedAllies = 0;
        int faintedEnemies = 0;
        int xpEarned = 0;
        int moneyEarned = 0;
        foreach(PersonajeSTO ally in aliados)
        {
            if(ally.fainted)
            {
                faintedAllies++;
            }
        }
        foreach(EnemigoSTO enemy in enemigos)
        {
            if(enemy.fainted)
            {
                faintedEnemies++;
            }
        }
        if(faintedAllies == 4)
        {
            SceneManager.LoadScene("World");
        }
        else if(faintedEnemies == 3)
        {
            aliados[0].earnXp(getTotalxp());
            aliados[1].earnXp(getTotalxp());
            aliados[2].earnXp(getTotalxp());
            aliados[3].earnXp(getTotalxp());
            print(getTotalxp()+ " earned");
            SceneManager.LoadScene("World");
        }
    }

    /// <summary>
    /// gets the total amount of money earned after battle
    /// </summary>
    /// <returns>total rewards</returns>
    public int getTotalReward()
    {
        int totalReward = 0;
        foreach(EnemigoSTO enemy in enemigos)
        {
            totalReward += enemy.money;
        }
        return totalReward;
    }
    
    /// <summary>
    /// gets the total amount of xp earned
    /// </summary>
    /// <returns>xp earned</returns>
    public int getTotalxp()
    {
        int totalXp = 0;
        foreach(EnemigoSTO enemy in enemigos)
        {
            totalXp += enemy.xp;
        }
        return totalXp;
    }

    /// <summary>
    /// attacks the ally
    /// </summary>
    /// <param name="index">ally to be damaged</param>
    /// <param name="attack">damage to be dealt</param>
    public static void damageAllies(int index, int attack)
    {
        allies[index].beDamaged(attack);
    }
    

    /// <summary>
    /// fades the text displayed if the player couldnt scape
    /// </summary>
    /// <returns></returns>
    public IEnumerator fadeText() 
    { 
        yield return new WaitForSeconds(1f);
        dialog.gameObject.SetActive(false);
        dialog.text = "";
    }

    /// <summary>
    /// attacks the enemy 
    /// </summary>
    /// <returns></returns>
    public IEnumerator attackAction()
    {
        GameObject attacker = lvlManager.GetCombatant(LevelManager.CurrentTurn); //gets the current ally's gameobject
        attacker.transform.Find("Animator").gameObject.GetComponent<Animator>().SetTrigger("attack"); //plays the attack animation
        Vector3 myPosition = attacker.transform.position; //saves last position of the ally
        attacker.transform.position = enemiesPositions[enemyIndex] - new Vector3(1.5f, 0, 0); //moves to attack
        yield return new WaitForSeconds(1.5f);
        attacker.transform.position = myPosition; //returns to original position
    }
}
