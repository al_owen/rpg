﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// pool of one kind of game object 
/// </summary>
public class GameObjectsPool : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab;

    public static GameObjectsPool Instance { get; private set; }
    private Queue<GameObject> objects = new Queue<GameObject>();

    private void Awake()
    {
        Instance = this;
        addObjects(6);
    }

    public GameObject Get()
    {
        if(objects.Count == 0)
        {
            addObjects(1);
        }
        return objects.Dequeue();
    }

    public void returnToPool(GameObject objectToReturn)
    {
        objectToReturn.SetActive(false);
        objects.Enqueue(objectToReturn);
    }

    private void addObjects(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var newObject = Instantiate(prefab);
            newObject.SetActive(false);
            objects.Enqueue(newObject);

            newObject.GetComponent<IGameObjectPooled>().Pool = this;
        }
    }
}
/// <summary>
/// interface to implement a pool
/// </summary>
public interface IGameObjectPooled
{
    GameObjectsPool Pool { get; set; }
}
