﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class InventarioSTO : ScriptableObject
{
    [SerializeField] private int money;
    public int Money { get => money; private set => money += value; }
    //int moneyHard;

    public PotionsSTO HealPotionS;
    public PotionsSTO HealPotionM;
    public PotionsSTO HealPotionL;

    public PotionsSTO EnergyPotionS;
    public PotionsSTO EnergyPotionM;
    public PotionsSTO EnergyPotionL;

    public PotionsSTO ElixirM;
    public PotionsSTO ElixirL;
    
    public PotionsSTO FenixTail;

    public ItemsSTO Scrap;

    

    /// <summary>
    /// returns an array of all items in the scriptable object
    /// </summary>
    /// <returns></returns>
    public ItemsSTO[] itemsArray()
    {
        ItemsSTO[] arrayOfItems = new ItemsSTO[10]{HealPotionS, HealPotionM, HealPotionL
        , EnergyPotionS, EnergyPotionM, EnergyPotionL, ElixirM, ElixirL, FenixTail, Scrap};
        return arrayOfItems;
    }

    public void earnMoney(int earnedMoney) => Money = earnedMoney;

    public bool spendMoney(int value)
    {
        if (money > value) {
            money -= value;
            return true;
        }
        return false;
    }

}
