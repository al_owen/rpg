﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// abilities scriptable objects 
/// </summary>
[CreateAssetMenu]
public class HabilidadesSTO : ScriptableObject
{
    public string Name;
    public Sprite imagen;
    public int damage;
    public int costeEnergia;
    EstadosSTO estado;
}
