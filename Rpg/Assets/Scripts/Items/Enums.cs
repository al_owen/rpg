﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums 
{
    
}

/// <summary>
/// effects enum
/// </summary>
public enum Effects
{
    heal, restoreEnergy, elixir, fenix
}

/// <summary>
/// talisman effects
/// </summary>
public enum TalismanEffects
{
    hp, damage, energy, armor, xp
}