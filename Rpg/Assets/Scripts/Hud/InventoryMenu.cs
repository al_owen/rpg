﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InventoryMenu : MonoBehaviour
{
    public InventarioSTO inventory;
    public ItemsSTO[] items = new ItemsSTO[15];
    public PersonajeSTO[] characters = new PersonajeSTO[4];
    public Text[] itemList = new Text[10];


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        InventoryUpdate();
    }

    public void changeTalisman(TalismanSTO newTalisman)
    {
        if (characters[0].talisman == newTalisman)
        {
            characters[0].removeTalisman();
        }
        else
        {
            characters[0].talismanSwap(newTalisman);
        }

    }

    public void InventoryUpdate()
    {
        for (int i = 0; i < itemList.Length; i++)
        {
            itemList[i].text = ""+items[i].quantity;
        }
    }

    public void usePotion(PotionsSTO poti)
    {
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].usePotion(poti);
        }
       
    }
}
