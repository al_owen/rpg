﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : MonoBehaviour, IMultipleGameObjectPooled
{
    
    public potionsTypes potiontype;
    public InventarioSTO inventory;
    private MultipleGameObjectsPool pool;
    public MultipleGameObjectsPool Pool
    {
        get { return pool; }
        set
        {
            if (pool == null)
            {
                pool = value;
            }
            else
            {
                throw new System.Exception("Bad pool use, this should only get set once!");
            }
        }
    }

    private void OnEnable()
    {
        spawn();
    }

    /// <summary>
    /// spawns potions randomly over the map
    /// </summary>
    public void spawn()
    {   
        this.transform.position = new Vector2(Random.Range(-36, 36), Random.Range(-23, 22));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Personaje")
        {
            if (this.potiontype == potionsTypes.HealPotionS) 
            {
                inventory.HealPotionS.quantity++;
            }
            else if (this.potiontype == potionsTypes.HealPotionM)
            {
                inventory.HealPotionM.quantity++;
            }
            else if (this.potiontype == potionsTypes.HealPotionL)
            {
                inventory.HealPotionL.quantity++;
            }
            else if (this.potiontype == potionsTypes.EnergyPotionS)
            {
                inventory.EnergyPotionS.quantity++;
            }
            else if (this.potiontype == potionsTypes.EnergyPotionM)
            {
                inventory.EnergyPotionM.quantity++;
            }
            else if (this.potiontype == potionsTypes.EnergyPotionL)
            {
                inventory.EnergyPotionL.quantity++;
            }
            else if (this.potiontype == potionsTypes.ElixirM)
            {
                inventory.ElixirM.quantity++;
            }
            else if (this.potiontype == potionsTypes.ElixirL)
            {
                inventory.ElixirL.quantity++;
            }
            else if (this.potiontype.Equals(potionsTypes.FenixTail))
            {
                inventory.FenixTail.quantity++;
            }
            pool.returnToPool(this.gameObject); //when picked up returns the potion to the pool
            pool.Get().SetActive(true);
        }
    }
}
/// <summary>
/// potions types
/// </summary>
public enum potionsTypes{
    HealPotionS, HealPotionM, HealPotionL, EnergyPotionS, EnergyPotionM, EnergyPotionL, ElixirM, ElixirL, FenixTail
}
