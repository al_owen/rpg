﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public MerchantsSTO merchant;
    public GameObject botonItem;
    private Text dialogo;
    public InventarioSTO inventario;
    private Text moneytxt;
    private int dialogueIndex;
    private Image vendorImage;

    private int dialoguesSize => merchant.dialogues.Length;
    private void Pause() => Time.timeScale = 0f;
    private void Resume() => Time.timeScale = 1f;

    // Start is called before the first frame update
    void Awake()
    {
        dialogo = transform.Find("Dialogo").gameObject.GetComponent<Text>();
        moneytxt = transform.Find("Ammount").gameObject.GetComponent<Text>();
        vendorImage = transform.Find("MercaderImg").gameObject.GetComponent<Image>();
    }

    private void OnEnable()
    {
        dialogueIndex = 0;
        vendorImage.sprite = merchant.merchantImage;
        moneytxt.text = "" + inventario.Money;
        dialogo.text = "Hola bienvenido a la tienda, en que puedo ayudarte";
    }

    public void talk()
    {
        dialogo.text = merchant.dialogues[dialogueIndex];
        if (dialogueIndex < dialoguesSize - 2)
        {
            dialogueIndex++;
        }
    }
    public void sellItem()
    {

    }
    public void buyItem(ItemsSTO itemBuyed)
    {

    }
    public void buyWindow()
    {

    }
    public void sellWindow()
    {

    }
    public void enterShop()
    {
        Pause();
        gameObject.SetActive(true);
    }
    public void exitShop()
    {
        StartCoroutine(enumeratorExit());
        Resume();
    }
    public void initButtons()
    {

    }
    public IEnumerator enumeratorExit()
    {
        dialogo.text = merchant.dialogues[dialoguesSize-1];
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
    }
}
