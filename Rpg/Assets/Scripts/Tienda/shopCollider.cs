﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shopCollider : MonoBehaviour
{
    public MerchantsSTO merchant;
    public GameObject botton;
    public Shop tienda;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Personaje")
        {
            tienda.merchant = this.merchant;
            botton.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Personaje")
        {
            botton.SetActive(false);
        }
    }


}
