﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsometricRenderer : MonoBehaviour
{

    public static string[] staticDirections = { "iddleN", "iddleNW", "iddleW", "iddleSW", "iddleS", "iddleSE", "iddleE", "iddleNE" };
    public static string[] runDirections = { "walkN", "walkNW", "walkW", "walkSW", "walkS", "walkSE", "walkE", "walkNE" };

    Animator _animator;
    int _lastDirection;

    private void Awake()
    {
        _animator = gameObject.GetComponent<Animator>();
    }

    /// <summary>
    /// sets the direction where the player will be facing
    /// </summary>
    /// <param name="direction"></param>
    public void setDirection(Vector2 direction)
    {
        string[] directionArray = null;

        if(direction.magnitude < 0.1f)
        {
            directionArray = staticDirections;
        }
        else
        {
            directionArray = runDirections;
            _lastDirection = directionIndex(direction, 8);
        }
        _animator.Play(directionArray[_lastDirection]);
    }


    //this function converts a Vector2 direction to an index to a slice around a circle
    //this goes in a counter-clockwise direction
    public int directionIndex(Vector2 dir, int sliceCount)
    {
        //get the normalized direction
        Vector2 normDir = dir.normalized;
        //calculate how many degrees one slice is
        float step = 360f / sliceCount;
        //claculate how many degrees half a slice is
        //we need this to offset the pie so that the North slice is aligned in the center
        float halfstep = step / 2;
        //get the angle from -180 to 180 of the direction vector relative to the up vector
        //this will return the angle between dir and north
        float angle = Vector2.SignedAngle(Vector2.up, normDir);
        //add the halfslice offset
        angle += halfstep;
        //if angle is negative then let's make it positive by adding 360 to wrap it around
        if(angle < 0)
        {
            angle += 360f;
        }
        //calculates the amount of steps required to reach this angle
        float stepCount = angle / step;
        //round it, and we have the answer, and i can finally stop commenting this!!
        return Mathf.FloorToInt(stepCount);
    }
}
