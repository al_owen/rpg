﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MerchantsSTO : ScriptableObject
{
    public Sprite merchantImage;
    public ItemsSTO[] items;
    [TextArea(4, 5)] public string[] dialogues;
}