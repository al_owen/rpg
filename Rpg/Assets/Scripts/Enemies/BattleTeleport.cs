﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class BattleTeleport : MonoBehaviour
{
    AudioSource audioSource;
    ParticleSystem particleSystem;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        particleSystem = this.GetComponent<ParticleSystem>();
    }

    private void OnEnable()
    {
        spawn();
    }

    public void spawn()
    {
        transform.position = new Vector3(Random.Range(-36, 36), Random.Range(-23,22));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(teleportToBattle());
    }

    public IEnumerator teleportToBattle()
    {
        particleSystem.Play();
        audioSource.Play();
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("BattleGround");
    }
    
}
