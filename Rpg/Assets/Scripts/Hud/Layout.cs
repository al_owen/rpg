﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// layout to save game
/// </summary>
[CreateAssetMenu]
public class Layout : ScriptableObject
{
    public string sceneName;
    public float sound;
    public Vector3 location;
    public Vector3 scale;
    public bool[] fainted;
    public int[] Hp;
    public int[] maxHp;
    public int[] energy;
    public int[] maxEnergy;
    public int[] xp;
    public int[] xpMax;
    public int[] inventoryQuantity;
    public int[] damage;
    public int[] armor;
    public TalismanSTO[] talisman;
    public int[] level;
    public int[] statPoints;
    public HabilidadesSTO[] abilitiesEzio;
    public HabilidadesSTO[] abilitiesAlfar;
    public HabilidadesSTO[] abilitiesAlexia;
    public HabilidadesSTO[] abilitiesScarlet;
}
