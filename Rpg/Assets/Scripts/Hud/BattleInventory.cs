﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class BattleInventory : MonoBehaviour
{

    public InventarioSTO inventario;
    public Text[] items;
    
    void awake()
    {
        items = new Text[9];
    }
    private void OnEnable()
    {
        textUpdate();
    }

    /// <summary>
    /// updates the quantity of the potions displayed in the menu
    /// </summary>
    public void textUpdate()
    {
        items[0].text = ""+inventario.HealPotionS.quantity;
        items[1].text = ""+inventario.HealPotionM.quantity;
        items[2].text = ""+inventario.HealPotionL.quantity;
        items[3].text = ""+inventario.EnergyPotionS.quantity;
        items[4].text = ""+inventario.EnergyPotionM.quantity;
        items[5].text = ""+inventario.EnergyPotionL.quantity;
        items[6].text = ""+inventario.ElixirM.quantity;
        items[7].text = ""+inventario.ElixirL.quantity;
        items[8].text = ""+inventario.FenixTail.quantity;
    }

}
