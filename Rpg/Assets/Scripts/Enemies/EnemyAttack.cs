﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public Turn myTurn;
    public EnemigoSTO character;
    public Vector3[] positions;
    Vector3 myPosition;
    Animator animator;
    public GameEventSTO endTurn;

    // Start is called before the first frame update
    void Start()
    {
        animator = this.transform.Find("Animator").gameObject.GetComponent<Animator>();
    }

    /// <summary>
    /// method that moves the enemy if not fainted
    /// </summary>
    public void Attack()
    {
        if (LevelManager.CurrentTurn == myTurn)
        {
            if (!character.fainted)
            {
                StartCoroutine(move());
            }
            else
            {
                endTurn.Raise();
            }
        }
    }

    /// <summary>
    /// corrutine for enemy movement
    /// </summary>
    /// <returns></returns>
    public IEnumerator move()
    {
        yield return new WaitForSeconds(1.5f);
        myPosition = this.transform.position;  //stores current position
        int attackPosition = Random.Range(0, positions.Length); //retrieves enemy position
        this.transform.position = positions[attackPosition]; //moves to an ally position
        Batalla.damageAllies(attackPosition, character.damage);//deals damage
        animator.SetTrigger("attack"); //attack animation
        yield return new WaitForSeconds(1f);
        this.transform.position = myPosition;
        endTurn.Raise(); //return to position and indicates end of turn
    }

}
