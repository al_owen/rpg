﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// updates the volume slider on load
/// </summary>
public class Volume : MonoBehaviour
{
    private Slider _slider;
    void Start()
    {
        _slider = this.gameObject.GetComponent<Slider>();
        _slider.value = DataManager.volumen;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
