﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public PersonajeSTO Character;
    public GameEventSTO changeStats;
    public GameEventSTO getDamaged;
    public GameEventSTO getXP;
    public GameEventSTO getEnergy;


    // Start is called before the first frame update
    void Start()
    {
        Character.fainted = false;
        changeStats.Raise();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Character.beDamaged(10);
            getDamaged.Raise();
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            Character.earnXp(10);
           // getXP.Raise();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Character.raiseEnergy(5);
            //getEnergy.Raise();
        }
    }

    public void restoreAll()
    {
        Character.restoreAll();
    }

    public void beDamaged(int damage)
    {
        Character.beDamaged(damage);
    }
    public void earnXp(int xp)
    {
        Character.earnXp(xp);
    }
    public void useEnergy(int energy) 
    {
        Character.useEnergy(energy);
    }
    public void heal(int hp)
    {
        Character.heal(hp);
    }
    public void restoreEnergy(int energy)
    {
        Character.restoreEnergy(energy);
    }
    public void raiseHp(int hp)
    {
        Character.raiseHp(hp);
    }
    public void raiseEnergy(int energy)
    {
        Character.raiseEnergy(energy);
    }
    public void levelUp()
    {
        Character.levelUp();
    }
}