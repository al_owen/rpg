﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleMenu : MonoBehaviour
{
    [SerializeField]private GameObject attackMenu, itemMenu;
    

    private void Start()
    {
        attackMenu = this.transform.Find("AttackMenu").gameObject;
        itemMenu = this.transform.Find("ItemsMenu").gameObject;
    }

    /// <summary>
    /// displays the attack menu
    /// </summary>
    public void displayAttackMenu()
    {
        attackMenu.SetActive(true);
    }

    /// <summary>
    /// displays the item menu
    /// </summary>
    public void displayitemMenu()
    {
        itemMenu.SetActive(true);
    }

    /// <summary>
    /// goes back to the battle menu
    /// </summary>
    public void backToBattleMenu()
    {
        if (attackMenu.activeSelf)
        {
            attackMenu.SetActive(false);
        }
        if (itemMenu.activeSelf)
        {
            itemMenu.SetActive(false);
        }
    }

}
