﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine;

public class Menu : MonoBehaviour
{
    Animator faderAnimator;
    AudioSource faderSound;
    private GameObject fader, settings;
    [SerializeField] private AudioMixer audioMixer;

    // Start is called before the first frame update
    void Start()
    {
        fader = this.gameObject.transform.Find("Fader").gameObject;
        settings = this.gameObject.transform.Find("SettingsMenu").gameObject;
        faderSound = fader.GetComponent<AudioSource>();
        faderAnimator = fader.GetComponent<Animator>();
    }

    /// <summary>
    /// starts a new game 
    /// </summary>
    public void newGame()
    {
        StartCoroutine(fadeEffect());
    }

    /// <summary>
    /// loads game
    /// </summary>
    public void continueGame(){
    
    }

    /// <summary>
    /// exits the game
    /// </summary>
    public void quitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// open the settings menu
    /// </summary>
    public void openSettings(){
        settings.SetActive(true);
    }

    /// <summary>
    /// closes the setting menu 
    /// </summary>
    public void closeSettings(){
        settings.SetActive(false);
    }
    
    /// <summary>
    /// plays a fade effect on the menu
    /// </summary>
    /// <returns></returns>
    private IEnumerator fadeEffect()
    {
        faderAnimator.SetTrigger("fadeIn");
        faderSound.Play();
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("World");
    }

    /// <summary>
    /// sets the volume
    /// </summary>
    /// <param name="volume">volume value</param>
    public void setVolume(float volume){
        audioMixer.SetFloat("Volume", volume);
    }
}
