﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ShowStats : MonoBehaviour
{
    public PersonajeSTO character;
    public Image characterImg;
    public Image hpTxt, energyTxt, expTxt;
    
}
