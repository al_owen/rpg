﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CharacterMenu : MonoBehaviour
{
    [SerializeField] private PersonajeSTO[] characters = new PersonajeSTO[4];
    public Image[] charactersImage = new Image[4];
    public Text[] stats = new Text[4];
    private GameObject statsMenu, inventarioMenu, skillsMenu, mapaMenu, currentMenu;
    void Start()
    {
        statsMenu = transform.Find("Stats").gameObject;
        inventarioMenu = transform.Find("Inventario").gameObject;
        skillsMenu = transform.Find("Skills").gameObject;
        mapaMenu = transform.Find("Mapa").gameObject;
        currentMenu = statsMenu;
    }
    
    private void OnEnable()
    {
        updateStats();
        currentMenu = statsMenu;
    }

    /// <summary>
    /// displays the stats of the characters
    /// </summary>
    public void updateStats()
    {
        for(int i = 0; i < 4; i++)
        {
            charactersImage[i].sprite = characters[i].body;
            stats[i].text = "HP: "+characters[i].hp+"/"+characters[i].maxHp+"\n SP: "+characters[i].energy+"/"+characters[i].maxEnergy
            +"\n XP: "+characters[i].xp+"/"+characters[i].xpMax;
        }
    }
    public void openMenu(int index)
    {
        currentMenu.SetActive(false);
        switch (index) {
            case 0:
                statsMenu.SetActive(true);
                currentMenu = statsMenu;
                break;
            case 1:
                inventarioMenu.SetActive(true);
                currentMenu = inventarioMenu;
                break;
            case 2:
                skillsMenu.SetActive(true);
                currentMenu = skillsMenu;
                break;
            case 3:
                mapaMenu.SetActive(true);
                currentMenu = mapaMenu;
                break;
        }
    }

}
