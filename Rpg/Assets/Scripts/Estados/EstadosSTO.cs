﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="status")]
public class EstadosSTO : ScriptableObject
{
    public string stateName;
    public Sprite image;
    public int damage;
    public int turns;

}
