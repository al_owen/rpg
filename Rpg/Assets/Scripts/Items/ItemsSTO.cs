﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// scriptable object of items
/// </summary>
[CreateAssetMenu (fileName = "items", menuName="ItemsSTO/items")]
public class ItemsSTO : ScriptableObject
{
    public Sprite icon;
    public string itemName;
    public int quantity;
    [Range(1, 999)] public int buyValue;
    [Range(1, 999)] public int sellValue;
}
