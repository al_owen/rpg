﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float speed = 1f;
    Rigidbody2D _rb;
    IsometricRenderer _irender;
    float _horizontal;
    float _vertical;


    void Start()
    {
        _rb = this.gameObject.GetComponent<Rigidbody2D>();
        _irender = this.gameObject.GetComponentInChildren<IsometricRenderer>();
    }

    private void Update()
    {
        _horizontal = Input.GetAxis("Horizontal");
        _vertical = Input.GetAxis("Vertical");
    }

    void FixedUpdate()
    {
        Vector2 currentPos = _rb.position;
        Vector2 inputVector = new Vector2(_horizontal, _vertical);
        inputVector = Vector2.ClampMagnitude(inputVector, 1);
        Vector2 movement = inputVector * speed;
        Vector2 newPos = currentPos + movement * Time.fixedDeltaTime;
        _rb.MovePosition(newPos);
        _irender.setDirection(movement);
    }


}
