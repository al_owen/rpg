﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ScriptableObject for enemies
/// </summary>
[CreateAssetMenu(fileName="Enemies",  menuName= "CharactersSTO/Enemies")]
public class EnemigoSTO : ScriptableObject
{
    public string enemyName;
    public int hp;
    public int maxHp;
    public int damage;
    public int xp;
    public int money;
    public bool fainted;
    //public int moneyHard;
    public GameEventSTO endTurn;
    public GameEventSTO changeEnemyStats;

    /// <summary>
    /// restores the enemies hp and puts fainted to false
    /// </summary>
    private void OnEnable()
    {
        restoreLife();
    }

    public void restoreLife()
    {
        hp = maxHp;
        fainted = false;
    }

    /// <summary>
    /// method that inflicts damage on the enemy
    /// </summary>
    /// <param name="damage">dealt damage</param>
    public void beDamaged(int damageTaken)
    {
        if (hp > damageTaken)
        {
            hp -= damageTaken;
        }
        else
        {
            hp = 0;
            fainted = true;
        }
        changeEnemyStats.Raise();
    }

}
