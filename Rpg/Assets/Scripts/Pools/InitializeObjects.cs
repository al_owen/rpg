﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeObjects : MonoBehaviour
{
    public MultipleGameObjectsPool potionPool;
    public GameObject enemy;

    // Start is called before the first frame update
    void Awake()
    {
        spawn();
        spawnEnemies();
    }

    /// <summary>
    /// object spawner
    /// </summary>
    private void spawn()
    {
        for (int i = 0; i<6; i++)
        {
            potionPool.Get().SetActive(true);
        }   
    }

    private void spawnEnemies()
    {
        for (int i = 0; i<4; i++)
        {
            Instantiate(enemy);
        }
    }
}
