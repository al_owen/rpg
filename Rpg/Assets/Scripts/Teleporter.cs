﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    ParticleSystem _ps;
    AudioSource tpSound;

    private void Start()
    {
        tpSound = this.gameObject.GetComponent<AudioSource>();
        _ps = this.gameObject.GetComponent<ParticleSystem>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Personaje") 
        {
            StartCoroutine(teleport());
        }
    }

    /// <summary>
    /// teleport coroutine
    /// </summary>
    /// <returns></returns>
    public IEnumerator teleport()
    {
        tpSound.Play();
        _ps.Play();
        yield return new WaitForSeconds(3f);
        //decides where to teleport
        if (this.gameObject.name == "TeleporterWorld")
        {
            SceneManager.LoadScene("Mercado");
        }
        else if(this.gameObject.name == "TeleporterMarket")
        {
            SceneManager.LoadScene("World");
        }
        else if (this.gameObject.tag == "Enemy")
        {
            SceneManager.LoadScene("BattleGround");
            DataManager.EnemyIndex = 1;
        }
    }
}
