﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    // stores who the current contestant is
    private static Turn currentTurn;
    public static Turn CurrentTurn { get => currentTurn; set => currentTurn = value; }
    
    //stores the combatants list
    private List<GameObject> combatants;
    public List<GameObject> Combatants { get => combatants; private set => combatants = value; }

    public GameEventSTO changeTurn;
    public GameObject[] enemies;
    public GameObject highlight;
    public EnemyBar[] enemyBars = new EnemyBar[3];

    [SerializeField] private GameObject actionsMenu;
    private int turn;
    private GameObject Ezio, Scarlet, Alfar, Alexia, enemy1, EnemyLeader, enemy2;

    // Start is called before the first frame update
    void Awake()
    {
        combatants = new List<GameObject>();
        Ezio = this.transform.Find("Ezio").gameObject;
        Scarlet = this.transform.Find("Scarlet").gameObject;
        Alfar = this.transform.Find("Alfar").gameObject;
        Alexia = this.transform.Find("Alexia").gameObject;
        turn = 0;
        LoadEnemies();
        currentTurn = Turn.Ezio;
        highlight.transform.position = combatants[turn].transform.position;
    }

    /// <summary>
    /// changes who the current combatant is
    /// </summary>
    public void nextTurn()
    {
        switch (turn)
        {
            case 0:
                currentTurn = Turn.Enemy1;
                turn = 1;
                break;
            case 1:
                currentTurn = Turn.Alfar;
                turn = 2;
                break;
            case 2:
                currentTurn = Turn.EnemyLeader;
                turn = 3;
                break;
            case 3:
                currentTurn = Turn.Scarlet;
                turn = 4;
                break;
            case 4:
                currentTurn = Turn.Enemy2;
                turn = 5;
                break;
            case 5:
                currentTurn = Turn.Alexia;
                turn = 6;
                break;
            case 6:
                currentTurn = Turn.Ezio;
                turn = 0;
                break;
        }
    }

    /// <summary>
    /// changes the turn and moves the spotlight 
    /// </summary>
    public void turnChange()
    {
        nextTurn();
        enableActions();
        changeTurn.Raise();
        Vector3 offset = combatants[turn].transform.position - new Vector3(0.2f, 0.5f,0);
        highlight.transform.position = offset;
    }

    /// <summary>
    /// loads the enemies that will fight
    /// </summary>
    public void LoadEnemies()
    {
        EnemyAttack enemyAtack;
        enemy1 = Instantiate(enemies[0], new Vector3(5, 3, 0), Quaternion.identity);
        enemyAtack = enemy1.gameObject.GetComponent<EnemyAttack>();
        enemyAtack.myTurn = Turn.Enemy1;
        enemyAtack.character.restoreLife();
        enemyBars[0].enemy = enemyAtack.character;
        EnemyLeader = Instantiate(enemies[1], new Vector3(5, 0, 0), Quaternion.identity);
        enemyAtack = EnemyLeader.gameObject.GetComponent<EnemyAttack>();
        enemyAtack.myTurn = Turn.EnemyLeader;
        enemyAtack.character.restoreLife();
        enemyBars[1].enemy = enemyAtack.character;
        enemy2 = Instantiate(enemies[2], new Vector3(5, -3, 0), Quaternion.identity);
        enemyAtack = enemy2.gameObject.GetComponent<EnemyAttack>();
        enemyAtack.myTurn = Turn.Enemy2;
        enemyAtack.character.restoreLife();
        enemyBars[2].enemy = enemyAtack.character;
        combatants.Add(Ezio);
        combatants.Add(enemy1);
        combatants.Add(Alfar);
        combatants.Add(EnemyLeader);
        combatants.Add(Scarlet);
        combatants.Add(enemy2);
        combatants.Add(Alexia);
    }

    /// <summary>
    /// gets the current combatant based on the turn
    /// </summary>
    /// <param name="combatantName">turn of the combatant that we want to be returned</param>
    /// <returns></returns>
    public GameObject GetCombatant(Turn combatantName)
    {
        switch(combatantName)
        {
            case Turn.Ezio:
                return Ezio;
            case Turn.Alfar:
                return Alfar;
            case Turn.Alexia:
                return Alexia;
            case Turn.Scarlet:
                return Scarlet;
            case Turn.Enemy1:
                return enemy1;
            case Turn.Enemy2:
                return enemy2;
            case Turn.EnemyLeader:
                return EnemyLeader;
        }
        return null;
    }

    /// <summary>
    /// enables and disables the actions menu based on the turn
    /// </summary>
    public void enableActions()
    {
        switch (CurrentTurn)
        {
            case Turn.Ezio:
            case Turn.Alfar:
            case Turn.Alexia:
            case Turn.Scarlet:
                actionsMenu.gameObject.SetActive(true);
                break;
            default:
                actionsMenu.gameObject.SetActive(false);
                break;
        }
    }
}

/// <summary>
/// turns available
/// </summary>
public enum Turn
{
    Ezio, Alfar, Scarlet, Alexia, Enemy1, Enemy2, EnemyLeader
}
