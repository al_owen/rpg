﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EzioStats : CharacterStats
{
    public InventarioSTO inventario;

    // Start is called before the first frame update
    void Start()
    {
        restoreAll();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Character.beDamaged(5);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {      
            Character.heal(20);
            
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Character.restoreEnergy(20);
        }
    }


    //spendMoney
    //earnMoney

}
