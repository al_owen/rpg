﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// scriptable object of the characters
/// </summary>
[CreateAssetMenu (fileName= "Allies", menuName= "CharactersSTO/Allies")]
public class PersonajeSTO : ScriptableObject
{
    public Sprite face;
    public Sprite body;
    public bool fainted;
    public bool target;
    public int hp;
    public int maxHp;
    public int damage;
    public int armor;
    public TalismanSTO talisman;
    public int energy;
    public int maxEnergy;
    public int level;
    public int xp;
    public int xpMax;
    public int[] stats;
    public int statPoints;
    public HabilidadesSTO[] abilities = new HabilidadesSTO[4];
    public GameEventSTO changeStats;
    public GameEventSTO potionUsed;


    /// <summary>
    /// method that allows us to restore all health and energy at the same time
    /// </summary>
    public void restoreAll()
    {
        hp = maxHp;
        energy = maxEnergy;
        fainted = false;
        changeStats.Raise();
    }

    /// <summary>
    /// method that inflicts damage on the player
    /// </summary>
    /// <param name="dealtDamage">dealt damage</param>
    public void beDamaged(int dealtDamage)
    {
        if ((hp + armor) > dealtDamage)
        {
            int finalDamage = dealtDamage - armor;
            if (dealtDamage > 0) { 
                hp -= finalDamage;
            }
        }
        else
        {
            hp = 0;
            fainted = true;
        }
        changeStats.Raise();
    }

    /// <summary>
    /// method that increases the player's xp
    /// </summary>
    /// <param name="xpEarned">xp earned</param>
    public void earnXp(int xpEarned)
    {
        int levelxp = (xp + xpEarned) - xpMax;
        if(levelxp < xpMax)
        {
            if ((xp + xpEarned) < xpMax)
            {
                xp += xpEarned;
            }
            else
            {
                levelUp();
                xp += levelxp;
            }
            changeStats.Raise();
        }
        else if(levelxp >= xpMax)
        {
            levelUp();
            earnXp(levelxp);
        }
       
    }

    /// <summary>
    /// method that uses the energy of the character
    /// </summary>
    /// <param name="usedEnergy">used energy</param>
    public bool useEnergy(int usedEnergy)
    {
        if (energy >= usedEnergy)
        {
            energy -= usedEnergy;
            changeStats.Raise();
            return true;
        }
        return false;
    }

    /// <summary>
    /// method that heals the character
    /// </summary>
    /// <param name="healthPoints">amount of hp to restore</param>
    public void heal(int healthPoints)
    {
        if ((hp + healthPoints) <= maxHp)
        {
            hp += healthPoints;
        }
        else
        {
            hp = maxHp;
        }
        changeStats.Raise();
    }

    /// <summary>
    /// method that restores some energy
    /// </summary>
    /// <param name="energyValue">amount of energy to restore</param>
    public void restoreEnergy(int energyValue)
    {
        if ((energy + energyValue) <= maxEnergy)
        {
            energy += energyValue;
        }
        else
        {
            energy = maxEnergy;
        }
        changeStats.Raise();
    }

    /// <summary>
    /// method that raises the max hp
    /// </summary>
    /// <param name="hpIncrease">hp amount</param>
    public void raiseHp(int hpIncrease)
    {
        maxHp += hpIncrease;
        hp += hpIncrease;
        changeStats.Raise();
    }

    /// <summary>
    /// method that raises the max energy
    /// </summary>
    /// <param name="energyIncrease">energy amount</param>
    public void raiseEnergy(int energyIncrease)
    {
        maxEnergy += energyIncrease;
        energy += energyIncrease;
        changeStats.Raise();
    }

    public void raiseXp(int xpIncrese)
    {
        xpMax += xpIncrese;
        xp = 0;
        changeStats.Raise();
    }

    /// <summary>
    /// method that levels up the character
    /// </summary>
    public void levelUp()
    {
        raiseHp((int)(maxHp*0.3));
        raiseEnergy(15);
        raiseXp(100);
        level++;
        damage += 10;
        statPoints++;
        changeStats.Raise();
    }

    /// <summary>
    /// method that uses a potion, if not possible, the method returns false;
    /// </summary>
    /// <param name="potion">Potion to be used</param>
    /// <returns></returns>
    public bool usePotion(PotionsSTO potion)
    {
        bool usedPotion = false;
        bool stock = false;
        stock = (potion.quantity > 0)? true : false;
        if (potion.effect == Effects.heal && hp < maxHp && stock)
        {
            heal(potion.restoreValue);
            usedPotion = true;
        }
        else if (potion.effect == Effects.restoreEnergy && energy < maxEnergy && stock)
        {
            restoreEnergy(potion.restoreValue);
            usedPotion = true;
        }
        else if (potion.effect == Effects.elixir && hp < maxHp && energy < maxEnergy && stock)
        {
            restoreEnergy(potion.restoreValue);
            heal(potion.restoreValue);
            usedPotion = true;
        }
        else if (potion.effect == Effects.fenix)
        {
            restoreAll();
            usedPotion = true;
        }

        if (usedPotion)
        {
            potion.quantity--;
            potionUsed.Raise();
        }
        return usedPotion;
    }

    /// <summary>
    /// Changes the current talisman
    /// </summary>
    /// <param name="newTalisman"></param>
    public void talismanSwap(TalismanSTO newTalisman)
    {
        if (talisman != null)
        {
            removeTalisman();
        }
        addTalisman(newTalisman);
    }

    /// <summary>
    /// adds a new Talisman
    /// </summary>
    /// <param name="newTalisman"></param>
    private void addTalisman(TalismanSTO newTalisman)
    {
        aplyTalismanEffect(newTalisman, false);
    }

    /// <summary>
    /// Removes the current talisman
    /// </summary>
    /// <returns>returns the old talisman</returns>
    public TalismanSTO removeTalisman()
    {
        TalismanSTO oldTalisman = talisman;
        aplyTalismanEffect(talisman, true);
        return oldTalisman;
    }

    /// <summary>
    /// Aplies or removes the talisman effects
    /// </summary>
    /// <param name="newTalisman">Talisman to be implemented</param>
    /// <param name="removeTalisman">Indicates whether the talisman will be implemented or removed</param>
    private void aplyTalismanEffect(TalismanSTO newTalisman, bool removeTalisman)
    {
        if(talisman != newTalisman && removeTalisman) return;

        int effectPoints = newTalisman.effect;
        talisman = newTalisman;
        if (removeTalisman) 
        {
            effectPoints = -newTalisman.effect;
            talisman = null;
        }

        switch (newTalisman.talismanEffects)
        {
            case TalismanEffects.hp:
                raiseHp(effectPoints);
                break;
            case TalismanEffects.damage:
                damage += effectPoints;
                break;
            case TalismanEffects.energy:
                raiseEnergy(effectPoints);
                break;
            case TalismanEffects.armor:
                armor += effectPoints;
                break;
            case TalismanEffects.xp:
                xpMax -= effectPoints;
                changeStats.Raise();
                break;
        }
    }
}
