﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// scriptable object of potions
/// </summary>
[CreateAssetMenu (fileName = "potions", menuName="ItemsSTO/potions")]
public class PotionsSTO : ItemsSTO
{
    public Effects effect;
    public int restoreValue;
}
