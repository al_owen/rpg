﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Hud : MonoBehaviour
{
    public PersonajeSTO character;
    public Image face;
    public Image effect;
    public Slider life;
    public Slider energy;
    public Slider xp;

    // Start is called before the first frame update
    void Start()
    {
        face.sprite = character.face;
        updateMaxStats();
        updateStats();
    }

    /// <summary>
    /// sets the max value of the slider based on the max hp, energy and xp
    /// </summary>
    public void updateMaxStats()
    {
        life.maxValue = character.maxHp;
        energy.maxValue = character.maxEnergy;
        xp.maxValue = character.xpMax;
    }

    /// <summary>
    /// updates the current value of health, energy and xp
    /// </summary>
    public void updateStats()
    {
        life.value = character.hp;
        energy.value = character.energy;
        xp.value = character.xp;
    }

    /// <summary>
    /// displays the effect 
    /// </summary>
    public void displayEffect()
    {
        //effect.sprite = character.item.icon;
        effect.gameObject.SetActive(true);
        StartCoroutine(disableEffect(1.5f));
    }

    /// <summary>
    /// disables the effect image
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    IEnumerator disableEffect(float time)
    {
        yield return new WaitForSeconds(time);
        effect.gameObject.SetActive(false);
    }
}
