﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private AudioMixer audioMixer;
    private GameObject pauseMenu;
    private GameObject settings;
    private GameObject menuPersonaje;
    private float lastVolume;

    [SerializeField] private GameObject objectsToSave;
    [SerializeField] private PersonajeSTO[] statsToSave;
    [SerializeField] private InventarioSTO inventory;
    [SerializeField] private Layout saveLayout;

    private void resume() => Time.timeScale = 1f;
    private void pause() => Time.timeScale = 0f;
    private void activate(GameObject menu) => menu.SetActive(true);
    private void deactivate(GameObject menu) => menu.SetActive(false);

    // Start is called before the first frame update
    void Start()
    {
        setVolume(DataManager.volumen);
        pauseMenu = this.gameObject.transform.Find("PauseMenu").gameObject;
        settings = this.pauseMenu.transform.Find("SettingsMenu").gameObject;
        menuPersonaje = this.gameObject.transform.Find("MenuPersonaje").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            quitCharacterMenu();
            if (!pauseMenu.gameObject.activeSelf)
            {
                pauseGame();
            }
            else
            {
                resumeGame();
            }
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            characterStats();

        }
    }

    /// <summary>
    /// opens the pause menu
    /// </summary>
    public void pauseGame()
    {
        activate(pauseMenu);
        pause();
    }

    /// <summary>
    /// resumes the pause menu
    /// </summary>
    public void resumeGame()
    {
        deactivate(pauseMenu);
        resume();
    }

    /// <summary>
    /// displays the allies stats
    /// </summary>
    public void stats() {
        deactivate(pauseMenu);
        activate(menuPersonaje);
    }

    /// <summary>
    /// quits character menu
    /// </summary>
    public void quitCharacterMenu()
    {
        if (menuPersonaje.gameObject.activeSelf)
        {
            deactivate(menuPersonaje);
        }
    }

    /// <summary>
    /// displays the characters stats
    /// </summary>
    public void characterStats()
    {
        if (!menuPersonaje.gameObject.activeSelf)
        {
            activate(menuPersonaje);
            pause();
        }
        else
        {
            deactivate(menuPersonaje);
            resume();
        }
    }

    /// <summary>
    /// opens the setting menu
    /// </summary>
    public void openSettings()
    {
        activate(settings);
    }

    /// <summary>
    /// closes the settings menu
    /// </summary>
    public void backToPause()
    {
        deactivate(settings);
    }

    /// <summary>
    /// sets the volume of the game
    /// </summary>
    /// <param name="volume">volume of the game</param>
    public void setVolume(float volume)
    {
        audioMixer.SetFloat("Volume", volume);
        lastVolume = volume;
        DataManager.volumen = volume;
    }

    /// <summary>
    /// goes bach to the main menu
    /// </summary>
    public void quitToMenu()
    {
        resume();
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// saves the game data
    /// </summary>
    public void saveGame(){
        saveLayout.sceneName = SceneManager.GetActiveScene().name;
        saveLayout.location = objectsToSave.transform.position;
        saveLayout.scale = objectsToSave.transform.localScale;
        saveLayout.sound = lastVolume;
        saveLayout.fainted = new bool[statsToSave.Length];
        saveLayout.Hp = new int[statsToSave.Length];
        saveLayout.maxHp = new int[statsToSave.Length];
        saveLayout.xp = new int[statsToSave.Length];
        saveLayout.xpMax = new int[statsToSave.Length];
        saveLayout.energy = new int[statsToSave.Length];
        saveLayout.maxEnergy = new int[statsToSave.Length];
        saveLayout.damage = new int[statsToSave.Length];
        saveLayout.armor = new int[statsToSave.Length];
        saveLayout.level = new int[statsToSave.Length];
        saveLayout.statPoints = new int[statsToSave.Length];
        saveLayout.talisman = new TalismanSTO[statsToSave.Length];
        saveLayout.abilitiesEzio = new HabilidadesSTO[statsToSave.Length];
        saveLayout.abilitiesAlfar = new HabilidadesSTO[statsToSave.Length];
        saveLayout.abilitiesScarlet = new HabilidadesSTO[statsToSave.Length];
        saveLayout.abilitiesAlexia = new HabilidadesSTO[statsToSave.Length];
        for(int i=0; i<statsToSave.Length; i++){
            saveLayout.fainted[i] = statsToSave[i].fainted;
            saveLayout.Hp[i] = statsToSave[i].hp;
            saveLayout.maxHp[i] = statsToSave[i].maxHp;
            saveLayout.xp[i] = statsToSave[i].xp;
            saveLayout.xpMax[i] = statsToSave[i].xpMax;
            saveLayout.energy[i] = statsToSave[i].energy;
            saveLayout.maxEnergy[i] = statsToSave[i].maxEnergy;
            saveLayout.damage[i] = statsToSave[i].damage;
            saveLayout.armor[i] = statsToSave[i].armor;
            saveLayout.level[i] = statsToSave[i].level;
            saveLayout.statPoints[i] = statsToSave[i].statPoints;
            saveLayout.talisman[i] = statsToSave[i].talisman;
            saveLayout.abilitiesEzio[i] = statsToSave[i].abilities[i];
            saveLayout.abilitiesAlfar[i] = statsToSave[i].abilities[i];
            saveLayout.abilitiesScarlet[i] = statsToSave[i].abilities[i];
            saveLayout.abilitiesAlexia[i] = statsToSave[i].abilities[i];
        }
        int size =this.inventory.itemsArray().Length;
        saveLayout.inventoryQuantity = new int[size];
        for(int i=0; i<size; i++)
        {
            saveLayout.inventoryQuantity[i] = this.inventory.itemsArray()[i].quantity;
        }

        string JsonText = JsonUtility.ToJson(saveLayout, true);
        File.WriteAllText("Assets/Save/SaveGame.json", JsonText);
        print("The game has been saved");
    }

    /// <summary>
    /// loads the game saved data
    /// </summary>
    public void loadGame(){
        string JsonText = File.ReadAllText("Assets/Save/SaveGame.json");
        JsonUtility.FromJsonOverwrite(JsonText, saveLayout);
        SceneManager.LoadScene(saveLayout.sceneName);
        resume();
        setVolume(saveLayout.sound);
        for(int i=0; i<statsToSave.Length; i++){
            statsToSave[i].fainted = saveLayout.fainted[i];
            statsToSave[i].hp = saveLayout.Hp[i];
            statsToSave[i].maxHp = saveLayout.maxHp[i];
            statsToSave[i].xp = saveLayout.xp[i];
            statsToSave[i].xpMax = saveLayout.xpMax[i];
            statsToSave[i].energy = saveLayout.energy[i];
            statsToSave[i].maxEnergy = saveLayout.maxEnergy[i];
            statsToSave[i].damage = saveLayout.damage[i];
            statsToSave[i].armor = saveLayout.armor[i];
            statsToSave[i].level = saveLayout.level[i];
            statsToSave[i].statPoints = saveLayout.statPoints[i];
            statsToSave[i].talisman = saveLayout.talisman[i];
            statsToSave[i].abilities[i] = saveLayout.abilitiesEzio[i];
            statsToSave[i].abilities[i] = saveLayout.abilitiesAlfar[i];
            statsToSave[i].abilities[i] = saveLayout.abilitiesScarlet[i];
            statsToSave[i].abilities[i] = saveLayout.abilitiesAlexia[i];
        }
        int size = this.inventory.itemsArray().Length;
        for(int i=0; i<size; i++)
        {
            this.inventory.itemsArray()[i].quantity = saveLayout.inventoryQuantity[i];
        }
        objectsToSave.transform.position = saveLayout.location;
        objectsToSave.transform.localScale = saveLayout.scale;
        print("The game has loaded");
    }

}
