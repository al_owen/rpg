﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EnemyBar : MonoBehaviour
{
    public EnemigoSTO enemy;
    public Slider slider;
    public Text text;

    // Start is called before the first frame update
    void Start()
    {
        text.text = enemy.enemyName;
        slider.maxValue = enemy.maxHp;
        updateStats();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// updates the enemy stats
    /// </summary>
    public void updateStats()
    {
        slider.value = enemy.hp; 
    }
}
