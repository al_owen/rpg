﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AttacksMenu : MonoBehaviour
{
    public PersonajeSTO[] characters = new PersonajeSTO[4];
    [SerializeField] private Text[] texts = new Text[4];
    [SerializeField] private Image[] images = new Image[4];

    private void OnEnable()
    {
        displayAttacks();
    }

    /// <summary>
    /// shows the attacks of the current turn
    /// </summary>
    public void displayAttacks()
    {
        switch (LevelManager.CurrentTurn)
        {
            case Turn.Ezio:
                updateAttacks(characters[0]);
                break;
            case Turn.Alfar:
                updateAttacks(characters[1]);
                break;
            case Turn.Scarlet:
                updateAttacks(characters[2]);
                break;
            case Turn.Alexia:
                updateAttacks(characters[3]);
                break;
        }
    }

    /// <summary>
    /// updates attacks of the current ally
    /// </summary>
    /// <param name="character"></param>
    public void updateAttacks(PersonajeSTO character)
    {
        for(int i = 0; i < 4; i++)
        {
            images[i].sprite = character.abilities[i].imagen;
            texts[i].text = character.abilities[i].Name;
        }
    }
}
