﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// class that handles the sounds
/// </summary>
public class MusicManager : MonoBehaviour
{
    public AudioClip[] scream;
    public AudioSource soundPlayer;
    
    private void Start()
    {
        scream = new AudioClip[10];
    }

    public void hurtSound()
    {
        soundPlayer.PlayOneShot(scream[0]);
    }
}
